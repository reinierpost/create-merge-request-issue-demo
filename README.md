# "Create merge request" issue demo

## Name
Create merge request issue demo

## Description
This project was created solely to check whether an issue we're experiencing on our self-hosted GitLab installation also occurs on gitlab.com. The instructions provided when creating a new issue tell us to do this.

## Usage

## Authors
Reinier Post, TU Eindhoven

## License
(c) 2023, TU Eindhoven

## Project status
Testing
